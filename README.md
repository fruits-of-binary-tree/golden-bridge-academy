## Project Name & Pitch

### Golden Bridge Academy

An application used as school managing system,  where you have 4 main roles:
- Headmaster - creates teams with students
- Teacher - creates courses, make student prefect, review homeworks, change status on homework, leave comments
- Prefect - creates homeworks, review other students homeworks and leave comments
- Student - creates homeworks, leave comments
  
Built with React, React Bootstrap, JavaScript, and CSS.

## Installation and Setup Instructions

Clone down this repository. You will need `node` and `npm` installed globally on your machine.  

Installation:

`npm install`  
 

To Start Server:

`npm start`  

To Visit App:

`localhost:3000`

## Reflection

This was a one month long project built during our final module at Telerik Academy. Project goals included using technologies learned up until this point and familiarizing ourselves with documentation for new features.  

Originally we wanted to build an application that helps a school to manage their employees and students.
We started this process by using the `create-react-app` boilerplate, then adding `react-router-`, `react-icons`, `react-loading`, `react-date-picker` 




