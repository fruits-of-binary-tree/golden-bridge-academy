/* eslint-disable react/prop-types */
/* eslint-disable max-len */
import React from 'react';
import './WithContainer.css';
import Container from 'react-bootstrap/esm/Container';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';


function WithContainer(Component) {
  return function wrapper(props) {
    return (
      <>
        <Header />
        <Container className="container-wrapper d-flex justify-content-center align-items-center">
          <Component {...props} />
        </Container>
        <Footer />
      </>
    );
  };
}

export default WithContainer;

