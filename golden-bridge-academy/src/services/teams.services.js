import {get, push, ref, update} from 'firebase/database';
import {db} from '../firebase-config';
import {objectToArray} from '../helpers/objectToArray';


export const addTeam = (name, students, courses) => {
  return push(
      ref(db, 'teams'),
      {
        name: name,
        students: students,
        courses: courses,
      },
  );
};

export const getAllTeams = () => {
  return get(ref(db, 'teams'))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }

        return objectToArray(snapshot.val());
      });
};

export const getTeamById = (teamId) => {
  return get(ref(db, `teams/${teamId}`))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }

        return (snapshot.val());
      });
};

export const updateTeamData = (teamId, updates) => {
  return update(ref(db, `teams/${teamId}`), updates);
};
