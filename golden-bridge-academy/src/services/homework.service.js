import {equalTo, get, orderByChild, push, query, ref, update} from 'firebase/database';
import {getDownloadURL, uploadBytes, ref as storageRef} from 'firebase/storage';
import {db, storage} from '../firebase-config';
import {objectToArray} from '../helpers/objectToArray';


export const addHomework = (title, content, file, studentId, courseId) => {
  return addToStorage(file)
      .then((snapshot) => getDownloadURL(snapshot.ref))
      .then((url) => updateHomework(title, content, url, studentId, courseId))
      .then((homeworkId) => updateDatabase(studentId, homeworkId.key, courseId)
          .then(() => homeworkId.key),
      );
};


const addToStorage = (file) => {
  if (file === null) {
    return;
  }

  const fileRef = storageRef(storage, `homeworks/${file.name}`);

  return uploadBytes(fileRef, file);
};

const updateHomework = (title, content, url, studentId, courseId) => {
  return push(
      ref(db, 'homeworks'),
      {
        title: title,
        content: content,
        url: url,
        author: studentId,
        courseId: courseId,
        status: 'Pending',
        comments: [],
      },
  );
};

const updateDatabase = (studentId, homeworkId, courseId) => {
  const updateObject = {};

  updateObject[`/students/${studentId}/homeworks/${homeworkId}`] = true;
  updateObject[`/courses/${courseId}/homeworks/${homeworkId}`] = true;


  return update(ref(db), updateObject);
};

export const updateDatabaseStatus = (status, homeworkId) => {
  const updateObject = {};

  updateObject[`/homeworks/${homeworkId}/status`] = status;

  return update(ref(db), updateObject);
};


export const getHomeworksByCourse = (courseId) => {
  return get(query(ref(db, 'homeworks'), orderByChild('courseId'), equalTo(courseId)))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }
        return (objectToArray(snapshot.val()));
      });
};

export const getHomeworkById = (homeworkId) => {
  return get(ref(db, `homeworks/${homeworkId}`))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }
        return {...snapshot.val(), comments: objectToArray(snapshot.val().comments)};
      });
};

export const getAllHomeworks = () => {
  return get(ref(db, `homeworks`))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }
        console.log(snapshot.val());
        return snapshot.val();
      });
};


