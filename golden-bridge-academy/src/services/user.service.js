import {get, set, ref, query, equalTo, orderByChild, update} from 'firebase/database';
import {db} from '../firebase-config';


export const getAllUsers = () => {
  return get(ref(db, `users`));
};

export const getUsersByReviewers = (isReviewer) => {
  return get(query(ref(db, `users`), orderByChild('isReviewer'), equalTo(isReviewer)));
};

export const getAllUsersByRole = (role) => {
  return get(query(ref(db, `users`), orderByChild('role'), equalTo(role)));
};

export const getUserByHandle = (handle) => {
  return get(ref(db, `users/${handle}`));
};

export const createUserHandle = (firstName, lastName, handle, email, phone, role, uid) => {
  const isReviewer = role === 'Teacher';

  return set(ref(db, `users/${handle}`), {
    firstName, lastName, handle, email, createdOn: new Date(), phone, role, uid, isReviewer,
  });
};

export const getUserData = (uid) => {
  return get(query(ref(db, `users`), orderByChild('uid'), equalTo(uid)));
};

export const updateUserProfilePicture = (handle, url) => {
  return update(ref(db), {
    [`users/${handle}/profilePicture`]: url,
  });
};

export const updateUserData = (uid, updates) => {
  return update(ref(db, `users/${uid}`), updates);
};

