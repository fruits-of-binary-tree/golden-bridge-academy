import {query, update, get, ref, push} from 'firebase/database';
import {db} from '../firebase-config';
import {objectToArray} from '../helpers/objectToArray';

export const addComment = (content, user, homeworkId, photo) => {
  return push(ref(db, `homeworks/${homeworkId}/comments`), {
    author: user, content, date: new Date().getTime(), photo: photo ? photo : '',
  });
};

export const getComments = (homeworkId) => {
  return get(query(ref(db, `homeworks/${homeworkId}/comments`)))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }
        return (objectToArray(snapshot.val()));
      });
};

export const deleteComment = (commentIndex, homeworkId) => {
  const updateObject = {};

  updateObject[`/homeworks/${homeworkId}/comments/${commentIndex}`] = null;

  return update(ref(db), updateObject);
};
