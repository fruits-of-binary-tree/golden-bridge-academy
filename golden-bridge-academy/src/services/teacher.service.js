import {get, push, ref, equalTo, orderByChild, update, query, limitToFirst} from 'firebase/database';
import {db} from '../firebase-config';


export const createTeacher = (uid) => {
  return push(ref(db, 'teachers'), {
    uid: uid,
    classes: {},
    courses: {},
    homework: {},
  });
};

export const getTeacherData = (uid) => {
  return get(query(ref(db, 'teachers'), orderByChild('uid'), equalTo(uid), limitToFirst(1)))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          throw new Error('Something went wrong!');
        }
        const teacherId = (Object.keys(snapshot.val())[0]);
        const teacherData = snapshot.val()[teacherId];
        return {teacherId, teacherData};
      });
};

export const getTeacherById = (teacherId) => {
  return get(ref(db, `teachers/${teacherId}`))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return {};
        }
        return (snapshot.val());
      });
};

export const updateTeacherData = (teacherId, updates) => {
  return update(ref(db, `teachers/${teacherId}`), updates);
};

