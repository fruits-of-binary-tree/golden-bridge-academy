import {get, push, ref, query, equalTo, orderByChild, update, limitToFirst} from 'firebase/database';
import {db} from '../firebase-config';
import {getUserData} from './user.service';


export const createStudent = (uid) => {
  return push(ref(db, 'students'), {
    uid,
    teamId: '',
    homework: {},
  });
};

export const getStudentData = (uid) => {
  return get(query(ref(db, 'students'), orderByChild('uid'), equalTo(uid), limitToFirst(1)))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          throw new Error('Something went wrong!');
        }
        const studentId = (Object.keys(snapshot.val())[0]);
        const studentData = snapshot.val()[studentId];
        return {studentId, studentData};
      });
};

export const getAllStudentsWithoutTeam = async () => {
  try {
    const snapshot = await get(query(ref(db, 'students'), orderByChild('teamId'), equalTo('')));

    if (!snapshot.exists()) {
      return [];
    }
    const students = Object.entries(snapshot.val());
    const modifiedStudents = await students.map(async ([studentId, student]) => {
      const user = await getUserData(student.uid);
      if (!user.exists()) {
        return {};
      }
      const [userData] = Object.values(user.val());
      return {...userData, isChecked: false, isSelected: false, studentId};
    });
    return Promise.all(modifiedStudents);
  } catch (e) {
    console.log(e.message);
  }
};


export const updateStudentData = (studentId, updates) => {
  return update(ref(db, `students/${studentId}`), updates);
};

export const getStudentById = (studentId) => {
  return get(ref(db, `students/${studentId}`))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return {};
        }
        return (snapshot.val());
      });
};
