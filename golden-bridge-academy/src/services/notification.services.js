import {get, push, ref, query, equalTo, orderByChild, update, onValue} from 'firebase/database';
import {db} from '../firebase-config';
import {objectToArray} from '../helpers/objectToArray';


export const createNotification = (receiverUid, message, date, seen, url) => {
  const notification = {
    receiverUid: receiverUid,
    message: message,
    date: date,
    seen: seen,
  };

  if (url) {
    notification.url = url;
  }

  return push(ref(db, 'notifications'), notification);
};

export const getNotificationsByReceiverUid = (receiverUid) => {
  return get(query(ref(db, 'notifications'), orderByChild('receiverUid'), equalTo(receiverUid)))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }
        return (objectToArray(snapshot.val()));
      });
};


export const deleteNotification = async (notificationId) => {
  const updateObject = {};

  updateObject[`/notifications/${notificationId}`] = null;

  return update(ref(db), updateObject);
};


export const setNotificationsAsSeen = async (notifications) => {
  const updateObject = {};

  for (const notification of notifications) {
    updateObject[`/notifications/${notification.id}/seen`] = true;
  }

  return update(ref(db), updateObject);
};


export const listenForNewNotifications = (receiverId, onNewNotifications) => {
  const notificationsRef = ref(db, '/notifications');
  onValue(notificationsRef, (snapshot) => {
    if (!snapshot.exists()) {
      return {};
    } else {
      const data = snapshot.val();
      const allNotifications = objectToArray(data);
      const notificationsForReceiver = allNotifications.filter((n) => n.receiverUid === receiverId);

      onNewNotifications(notificationsForReceiver.reverse());
    }
  });
};


