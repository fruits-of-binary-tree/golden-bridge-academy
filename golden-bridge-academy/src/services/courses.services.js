import {equalTo, query, update, get, orderByChild, push, ref} from 'firebase/database';
import {db, storage} from '../firebase-config';
import {getDownloadURL, uploadBytes, ref as storageRef} from 'firebase/storage';
import {objectToArray} from '../helpers/objectToArray';

export const addCourse = (teacherId, name, objectives, teamId, image) => {
  return addToStorage(image)
      .then((snapshot) => getDownloadURL(snapshot.ref)
          .then((url) => addCourseInfo(teacherId, name, objectives, teamId, url),
          ),
      );
};


const addToStorage = (file) => {
  if (file === null) {
    return;
  }

  const fileRef = storageRef(storage, `courseImages/${file.name}`);

  return uploadBytes(fileRef, file);
};

const addCourseInfo = (teacherId, name, objectives, teamId, url) => {
  return push(
      ref(db, 'courses'),
      {
        teacherId: teacherId,
        name: name,
        objectives: objectives,
        teamId: teamId,
        url: url,
      },
  ).then((result) => {
    const updateObject = {};
    const courseId = result.key;

    updateObject[`/teachers/${teacherId}/courses/${courseId}`] = true;
    updateObject[`/teams/${teamId}/courses/${courseId}`] = true;

    return update(ref(db), updateObject);
  });
};


export const getCoursesByTeacher = (teacherId) => {
  return get(query(ref(db, 'courses'), orderByChild('teacherId'), equalTo(teacherId)))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return [];
        }
        return (objectToArray(snapshot.val()));
      });
};


export const getCoursesById = (courseId) => {
  return get(ref(db, `courses/${courseId}`))
      .then((snapshot) => {
        if (!snapshot.exists()) {
          return {};
        }
        return ({...snapshot.val(), id: courseId});
      });
};

export const updateCourse = (courseId, updates) => {
  return update(ref(db, `courses/${courseId}`), updates);
};

export const deleteCourse = async (courseId, teamId, teacherId, courseHomeworks) => {
  const updateObject = {};

  updateObject[`/teams/${teamId}/courses/${courseId}`] = null;
  updateObject[`/teachers/${teacherId}/courses/${courseId}`] = null;
  updateObject[`/courses/${courseId}`] = null;

  courseHomeworks.forEach((courseHomework) => {
    updateObject[`/homeworks/${courseHomework.id}`] = null;
  });

  return update(ref(db), updateObject);
};


