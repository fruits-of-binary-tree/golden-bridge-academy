import React from 'react';
import {useNavigate} from 'react-router-dom';
import './RegisterForm.css';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';


function RegisterForm(props) {
  const navigate = useNavigate();

  const goBack = () => {
    navigate('/home');
  };

  const login = () => {
    navigate('/login');
  };

  return (
    <>
      <div className="d-flex justify-content-center">
        <div className="md-auto">
          <Form className="login-form m-5">

            <Form.Group controlId='formFirstName'>
              <Form.Label>First Name</Form.Label>
              <Form.Control value={props.state.firstName} onChange={(e) => props.update('firstName', e)} type='text' placeholder='Your first name goes here'></Form.Control>
            </Form.Group>
            <Form.Group controlId='formLastName'>
              <Form.Label>Last Name</Form.Label>
              <Form.Control value={props.state.lastName} onChange={(e) => props.update('lastName', e)} type='text' placeholder='Your last name goes here'></Form.Control>
            </Form.Group>
            <Form.Group controlId='formUsername'>
              <Form.Label>Username</Form.Label>
              <Form.Control value={props.state.username} onChange={(e) => props.update('username', e)} type='text' placeholder='Your username goes here'></Form.Control>
            </Form.Group>
            <Form.Group controlId='formEmail'>
              <Form.Label>Email</Form.Label>
              <Form.Control value={props.state.email} onChange={(e) => props.update('email', e)} type='email' placeholder='@example.com'></Form.Control>
            </Form.Group>
            <Form.Group controlId='formPassword'>
              <Form.Label>Password</Form.Label>
              <Form.Control value={props.state.password} onChange={(e) => props.update('password', e)} type='password' placeholder='Your password goes here'></Form.Control>
            </Form.Group>
            <Form.Group controlId='formPhone'>
              <Form.Label>Phone Number</Form.Label>
              <Form.Control value={props.state.phone} onChange={(e) => props.update('phone', e)} type='text' placeholder='Your phone number goes here'></Form.Control>
            </Form.Group>
            <Form.Label>Role</Form.Label>
            <Form.Select aria-label="Default select example" onChange={(e) => props.update('role', e)} >
              <option>Choose Role</option>
              <option value='Teacher'>Teacher</option>
              <option value='Student'>Student</option>
            </Form.Select>
            <div className="text-center mt-2">
              <div>
                <Button className="m-2" onClick={props.register} variant='primary' type='submit'>Create Account</Button>
                <Button className="m-2" onClick={login} variant='secondary' type='submit'>Already have an account</Button>
              </div>
              <div className="text-center">
                <Button className="mt-2" onClick={goBack} variant='secondary' type='submit'>Cancel</Button>
              </div>
            </div>
          </Form>
        </div>
      </div >
    </>
  );
}

export default RegisterForm;

