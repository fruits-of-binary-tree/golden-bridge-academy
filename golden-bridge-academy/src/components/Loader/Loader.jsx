import './Loader.css';
import React from 'react';
import ReactLoading from 'react-loading';

const Loader = ({type, color}) => (
  <div className="loader-container">
    <ReactLoading className="loader" type={type} color={color} height={50} width={50} />
  </div>
);

export default Loader;

