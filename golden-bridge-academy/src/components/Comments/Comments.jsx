import React from 'react';
import {useContext, useState} from 'react';
import AppContext from '../../providers/app-context';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {addComment, deleteComment, getComments} from '../../services/comments.service.js';
import SingleComment from './SingleComment';

function Comments(props) {
  const [inputComment, setInputComment] = useState('');
  const {appState, setLoading} = useContext(AppContext);


  const handleInputComment = () => {
    setLoading(true);
    addComment(inputComment, appState.userData.handle, props.id, appState.userData?.profilePicture)
        .then(() => {
          setInputComment('');
          getComments(props.id)
              .then((comments) => {
                props.setHomework({
                  ...props.homework,
                  comments: comments,
                });
              })
              .catch((e) => {
                console.log(e.message);
              })
              .finally(() => {
                setLoading(false);
              });
        });
  };


  const handleDelete = (comment, homeworkId) => {
    setLoading(true);
    deleteComment(comment.id, homeworkId)
        .then(() => {
          getComments(props.id)
              .then((comments) => {
                props.setHomework({
                  ...props.homework,
                  comments: comments,
                });
              })
              .catch((e) => {
                console.log(e.message);
              })
              .finally(() => {
                setLoading(false);
              });
        });
  };


  return (
    <>
      <div className='d-flex align-items-center'>
        <Form.Control style={{height: '90%'}} value={inputComment} onChange={(e) => setInputComment(e.target.value)} type='text' placeholder='Write your comment here'></Form.Control>
        <Button className="m-2" variant='primary' onClick={handleInputComment}>Publish</Button>

      </div>
      <div>
        {props.homework?.comments.map((comment, i) => <SingleComment handleDelete={handleDelete} homeworkId={props.id} homework={props.homework} comment={comment} key={comment.id} />)}
      </div>
    </>
  );
}

export default Comments;
