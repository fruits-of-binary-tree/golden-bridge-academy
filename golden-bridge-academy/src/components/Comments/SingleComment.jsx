import React, {useContext} from 'react';
import Container from 'react-bootstrap/Container';
import {AiTwotoneDelete} from 'react-icons/ai';
import AppContext from '../../providers/app-context';
import './SingleComment.css';


function SingleComment(props) {
  const {appState} = useContext(AppContext);
  const date = props.comment.date;

  const year = new Date(date).getFullYear();
  const month = ('0' + (new Date(date).getMonth() + 1)).slice(-2);
  const day = ('0' + new Date(date).getDate()).slice(-2);

  return (
    <Container className='mb-2 comment d-flex'>
      <img src={props.comment?.photo} className='rounded-circle profile-img'/>
      <h6 className='author m-1 d-flex align-items-center'>By: {props.comment.author}</h6>
      <p className='content m-1 d-flex align-items-center'>{props.comment.content}</p>
      <p className='date  m-1 d-flex align-items-center justify-content-end'>{day}/{month}/{year}</p>
      {props.comment.author === appState.userData.handle &&
            <i className='ml-1 d-flex align-items-center justify-content-end' onClick={() => props.handleDelete(props.comment, props.homeworkId)}><AiTwotoneDelete /></i>
      }
    </Container>
  );
}

export default SingleComment;
