import React from 'react';
import './Footer.css';
import {AiOutlineCopyrightCircle} from 'react-icons/ai';
import {SiGitlab} from 'react-icons/si';
import Container from 'react-bootstrap/Container';


function Footer() {
  return (
    <Container fluid="ml" className="">
      <div className="footer">

        <div className="contact">
          <h6>Contact | </h6>
          <span className='profile nina-profile'>
            <i><SiGitlab /></i>
            <a href='https://gitlab.com/nina_stoyanova'>Nina Stoyanova&apos;s profile</a>
          </span>
          <span className='profile natalia-profile'>
            <i><SiGitlab /></i>
            <a href='https://gitlab.com/NataliaZaharieva'>Natalia Zaharieva&apos;s profile</a>
          </span>
          <h6 className="connect">Connect | </h6>
          <a href='https://gitlab.com/f4504/beauty-answers.git'>GitLab repository</a>
        </div>

        <span className="copyright">
          <i><AiOutlineCopyrightCircle /></i>
          <p>Copyright 2022. All rights reserved</p>
        </span>

      </div>
    </Container>
  );
}

export default Footer;
