import {useContext} from 'react';
import AppContext from '../../providers/app-context';
import {NavLink} from 'react-router-dom';
import {useNavigate} from 'react-router-dom';
import {logoutUser} from '../../services/auth.service';
import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Offcanvas from 'react-bootstrap/Offcanvas';
import './Header.css';
import {MdOutlineNotificationsActive, MdOutlineNotificationsNone} from 'react-icons/md';
import noUser from '../../images/no-user.png';


function Header() {
  const {appState, setContext, notificationsState} = useContext(AppContext);
  const navigate = useNavigate();

  const logout = () => {
    logoutUser()
        .then(() => {
          setContext({user: null, userData: null});
          navigate('/home');
        });
  };

  const redirect = () => {
    navigate('/home');
  };

  const goToNotifications = () => {
    navigate('/notifications');
  };

  const goToProfile = () => {
    navigate('/profile');
  };

  return (
    <Navbar expand="ml" className="header p-0" variant="light">
      <img className='logo' onClick={redirect} src="https://www.freepnglogos.com/uploads/hogwarts-logo-png/logo-hogwarts-hogwartsessence-deviantart-19.png" width="50" alt="logo hogwarts" />
      {appState && appState.userData && appState.userData.firstName ?
        <>
          <div className="d-flex header-user-name m-3 align-items-center">
            {appState.userData.profilePicture ?
              <img src={appState.userData.profilePicture} className='rounded-circle header-img border border-light' onClick={goToProfile} style={{cursor: 'pointer'}}></img> :
              <img className='rounded-circle header-img border border-light' onClick={goToProfile} style={{cursor: 'pointer'}} src={noUser} ></img >
            }
            <div>{`${appState.userData.firstName} ${appState.userData.lastName}`}</div>
            {appState?.userData?.role !== 'Headmaster' ?
              notificationsState && notificationsState.find((n) => n.seen === false) ?
                <i><MdOutlineNotificationsActive style={{cursor: 'pointer'}} className="m-3" size={25} onClick={goToNotifications} /></i> :
                <i><MdOutlineNotificationsNone style={{cursor: 'pointer'}} className="m-3" size={25} onClick={goToNotifications} /> </i> :
              null
            }
          </div>
        </> :
        null
      }

      <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-ml`} />

      <Navbar.Offcanvas
        id={`offcanvasNavbar-expand-ml`}
        aria-labelledby={`offcanvasNavbarLabel-expand-ml`}
        placement="end" >

        <Offcanvas.Header closeButton className="text-light">
          <Offcanvas.Title id={`offcanvasNavbarLabel-expand-ml`}>Menu</Offcanvas.Title>
        </Offcanvas.Header>

        <Offcanvas.Body >
          <Nav className="justify-content-end flex-grow-1 pe-3 bg-dark text-white">
            {appState.user !== null ?
              <>
                <NavLink to="/profile" className="text-decoration-none text-white nav-link">Profile</NavLink>
                {appState?.userData?.role === 'Teacher' &&
                  <NavLink to="/homeworks" className="text-decoration-none text-white nav-link">Homeworks</NavLink>
                }
                {appState?.userData?.isReviewer === true && appState?.userData?.role === 'Student' &&
                  <NavLink to="/review-homeworks" className="text-decoration-none text-white nav-link">Grade Homeworks</NavLink>
                }

                {appState?.userData?.role !== 'Headmaster' &&
                  <NavLink to="/notifications" className="text-decoration-none text-white nav-link">Notifications</NavLink>
                }

                <button onClick={logout} className="border-0 text-white bg-dark logout-btn nav-link">Logout</button>
              </> :
              <>
                <NavLink to="/register" className="text-decoration-none text-white nav-link">Register</NavLink>
                <NavLink to='/login' className='text-decoration-none text-white nav-link'>Login</NavLink>
              </>
            }
          </Nav >
        </Offcanvas.Body >
      </Navbar.Offcanvas >
    </Navbar >
  );
}

export default Header;
