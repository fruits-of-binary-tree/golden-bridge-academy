import React, {useContext, useEffect, useState} from 'react';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import {AiOutlineCheck} from 'react-icons/ai';
import {AiOutlineUsergroupAdd} from 'react-icons/ai';
import './Search.css';
import {updateUserData} from '../../services/user.service';
import AppContext from '../../providers/app-context';
import {toast} from 'react-toastify';


function Search(props) {
  const {setLoading} = useContext(AppContext);
  const [showResults, setShowResults] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [filteredStudents, setFilteredStudents] = useState([]);


  const onFocusHandler = () => {
    setShowResults(!showResults);
  };

  const onBlurHandler = () => {
    setShowResults(false);
  };

  const handleCheck = (uid) => {
    const modifiedStudents = filteredStudents.map((student) => {
      const modifiedStudent = {...student};
      if (modifiedStudent.uid === uid) {
        modifiedStudent.isChecked = !modifiedStudent.isChecked;
      }
      return modifiedStudent;
    });
    setFilteredStudents(modifiedStudents);
  };

  const addMembers = () => {
    props.setStudents(filteredStudents.map((student) => {
      const modifiedStudent = {...student};

      if (student.isChecked) {
        modifiedStudent.isSelected = true;
      }
      return modifiedStudent;
    }));
  };

  const createReviewer = () => {
    setLoading(true);
    Promise.all(filteredStudents.map((student) => {
      const modifiedStudent = {...student};

      if (student.isChecked) {
        modifiedStudent.isSelected = true;
        return updateUserData(student.handle, {isReviewer: true});
      }
    }))
        .then(() => {
          toast.success(`You succesffully added new prefect/s`, {position: toast.POSITION.BOTTOM_RIGHT, icon: '🧙‍♂️'});
          props.setReviewersAdded((prev) => prev + 1);
        })
        .catch((e) => {
          console.log(e);
        })
        .finally(() => {
          setLoading(false);
        });
  };

  useEffect(() => {
    filterStudents(searchTerm, props.students);
  }, [props.students, searchTerm]);


  const filterStudents = (searchTerm, students) => {
    if (students?.length > 0) {
      const filtered = students.map((student) => {
        const modifiedStudents = {...student};
        modifiedStudents.filtered = false;

        if (searchTerm === '') {
          modifiedStudents.filtered = true;
        } else if (student.handle.toLowerCase().includes(searchTerm.toLowerCase())) {
          modifiedStudents.filtered = true;
        }
        return modifiedStudents;
      });
      setFilteredStudents(filtered);
    } else {
      setFilteredStudents([]);
    }
  };


  return (
    <Container className="search-container" onMouseLeave={onBlurHandler}>
      <Form className="search ">
        <Form.Group controlId='formFirstName' className="input">
          <Form.Control className="w-75" autoComplete="off" type='text' placeholder='Search...' onFocus={onFocusHandler} onChange={(e) => {
            setSearchTerm(e.target.value);
          }}></Form.Control>
        </Form.Group>
        <Button className="mb-3 search-btn w-25" variant='primary' onClick={props.userRole === 'Headmaster' ? addMembers : createReviewer}>
          {props.userRole === 'Headmaster' ? <AiOutlineUsergroupAdd /> : 'Add'}
        </Button>
      </Form>
      {showResults && filteredStudents.some((s) => s.filtered && !s.isSelected) &&
                <div className='search-results' style={props.userRole === 'Headmaster' ? {width: '52%'} : {width: '54%'}}>
                  {filteredStudents.map((student) => {
                    return student.filtered && !student.isSelected && (
                      <div key={student.uid} className="search-suggestion" onClick={() => handleCheck(student.uid)}>
                        {student.isChecked && <i><AiOutlineCheck /></i>}
                        <p>{student.firstName} {student.lastName}</p>
                      </div>
                    );
                  })}
                </div>
      }
    </Container>
  );
}

export default Search;
