import './App.css';
import React, {useEffect, useRef, useState} from 'react';
import FOG from 'vanta/dist/vanta.fog.min';
import AppContext from './providers/app-context';
import Register from './pages/Register/Register';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import {useAuthState} from 'react-firebase-hooks/auth';
import {auth} from './firebase-config';
import NotFound from './components/NotFound/NotFound';
import Login from './pages/Login/Login';
import {getUserData} from './services/user.service';
import Home from './pages/Home/Home';
import LandingPage from './pages/LandingPage/LandingPage';
import 'bootstrap/dist/css/bootstrap.min.css';
import Profile from './pages/Profile/Profile';
import StudentHomePage from './pages/Home/StudentHomePage';
import TeacherHomePage from './pages/Home/TeacherHomePage';
import HeadmasterHomePage from './pages/Home/HeadmasterHomePage';
import CreateCourse from './pages/CreateCourse/CreateCourse';
import DetailedCourse from './pages/DetailedCourse/DetailedCourse';
import {getStudentData} from './services/student.service';
import {getTeacherData} from './services/teacher.service';
import Homeworks from './pages/Homeworks/Homeworks';
import CreateHomework from './pages/Homeworks/CreateHomework';
import DetailedHomework from './pages/Homeworks/DetailedHomework';
import Loader from './components/Loader/Loader';
import {listenForNewNotifications} from './services/notification.services';
import Notifications from './pages/Notifications/Notifications';
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


function App() {
  const [vantaEffect, setVantaEffect] = useState(0);
  const myRef = useRef(null);
  const [loading, setLoading] = useState(false);
  const [appState, setAppState] = useState({
    user: null,
    userData: null,
  });

  const [studentState, setStudentState] = useState(null);
  const [teacherState, setTeacherState] = useState(null);
  const [notificationsState, setNotificationsState] = useState(null);
  const [user, load] = useAuthState(auth);

  useEffect(() => {
    setLoading(true);
    if (user === null) {
      return;
    }

    getUserData(user.uid)
        .then((snapshot) => {
          if (!snapshot.exists()) {
            throw new Error('Something went wrong!');
          }
          setAppState({
            user,
            userData: snapshot.val()[Object.keys(snapshot.val())[0]],
          });
        })
        .catch((e) => {
          console.log(e.message);
        })
        .finally(() => {
          setLoading(false);
        });
  }, [user]);


  useEffect(() => {
    setLoading(true);
    if (user === null) {
      return;
    }

    if (appState && appState.userData && appState.userData.role === 'Student') {
      getStudentData(user.uid)
          .then((student) => {
            setStudentState(student);
          })
          .catch((e) => {
            console.log(e.message);
          })
          .finally(() => {
            setLoading(false);
          });
    } else if (appState && appState.userData && appState.userData.role === 'Teacher') {
      getTeacherData(user.uid)
          .then((teacher) => {
            setTeacherState(teacher);
          })
          .catch((e) => {
            console.log(e.message);
          })
          .finally(() => {
            setLoading(false);
          });
    }
  }, [appState.userData]);


  useEffect(() => {
    setLoading(true);
    if (appState.userData === null) {
      return;
    }
    listenForNewNotifications(appState?.userData?.uid, (notifications) => {
      setNotificationsState(notifications);
    });
  }, [appState.userData]);

  useEffect(() => {
    if (!vantaEffect) {
      setVantaEffect(FOG({
        el: myRef.current,
        mouseControls: true,
        touchControls: true,
        gyroControls: false,
        minHeight: 200.00,
        minWidth: 200.00,
        highlightColor: 0x0,
        midtoneColor: 0x4a4747,
        lowlightColor: 0x484e7a,
        baseColor: 0x615c5c,
        blurFactor: 0.60,
        speed: 1.00,
      }));
    }
    return () => {
      if (vantaEffect) vantaEffect.destroy();
    };
  }, [vantaEffect]);


  return (
    <div className='app' ref={myRef}>
      <ToastContainer />
      <BrowserRouter>
        <AppContext.Provider value={{
          appState: appState,
          setContext: setAppState,
          studentState: studentState,
          setStudentState: setStudentState,
          setLoading: setLoading,
          teacherState: teacherState,
          setTeacherState: setTeacherState,
          notificationsState: notificationsState,
          setNotificationsState: setNotificationsState,
        }}>
          {/* presentational routes */}
          {load ?
            <>
              {loading && <Loader type={'spinningBubbles'} color={'#FFFFFF'} />}
            </> :
            <Routes>
              <Route path='' element={<LandingPage />}></Route>
              {appState.userData?.role === 'Student' &&
                <Route path='home' element={<StudentHomePage />}></Route>
              }
              {appState.userData?.role === 'Teacher' &&
                <Route path='home' element={<TeacherHomePage />}></Route>
              }
              {appState.userData?.role === 'Headmaster' &&
                <Route path='home' element={<HeadmasterHomePage />}></Route>
              }
              <Route path='home' element={<Home />}></Route>
              {/* not registered user route */}
              <Route path='profile' element={<Profile />}></Route>
              {appState?.userData?.role === 'Teacher' ?
                <Route path='/homeworks' element={<Homeworks />}></Route> :
                <Route path='courses/:id/homeworks' element={<Homeworks />}></Route>
              }

              {appState?.userData?.role === 'Student' &&
                <Route path='/review-homeworks' element={<Homeworks />}></Route>
              }
              <Route path='notifications' element={<Notifications />}></Route>
              <Route path=':id/create-homework' element={<CreateHomework />}></Route>
              <Route path=':id/detailed-homework' element={<DetailedHomework />}></Route>
              <Route path='teachers/id/create-course' element={<CreateCourse />}></Route>
              <Route path='courses/:id/detailed-course' element={<DetailedCourse />}></Route>
              <Route path='login' element={<Login />}></Route>
              <Route path='register' element={<Register />}></Route>
              <Route path='*' element={<NotFound />}></Route>
            </Routes>
          }
        </AppContext.Provider>
      </BrowserRouter>
    </div >
  );
}

export default App;
