import React, {useContext, useEffect, useState} from 'react';
import WithContainer from '../../hoc/WithContainer/WithContainer';
import {useParams, useNavigate} from 'react-router-dom';
import {deleteCourse, getCoursesById} from '../../services/courses.services';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import {getUserByHandle} from '../../services/user.service';
import AppContext from '../../providers/app-context';
import './DetailedCourse.css';
import {getHomeworksByCourse} from '../../services/homework.service';

function DetailedCourse() {
  const {appState, setLoading} = useContext(AppContext);
  const [detailedCourse, setDetailedCourse] = useState(null);
  const {id} = useParams();
  const [detailedCourseUser, setDetailedCourseUser] = useState({});
  const navigate = useNavigate();

  useEffect(() => {
    setLoading(true);
    getCoursesById(id)
        .then((course) => {
          setDetailedCourse(course);
        })
        .catch((e) => {
          console.log(e.message);
        })
        .finally(() => {
          setLoading(false);
        });
  }, []);

  useEffect(() => {
    setLoading(true);
    if (appState && appState.userData) {
      getUserByHandle(appState.userData.handle)
          .then((snapshot) => {
            const user = snapshot.val();
            setDetailedCourseUser(user);
          })
          .catch((e) => {
            console.log(e.message);
          })
          .finally(() => {
            setLoading(false);
          });
    }
  }, [detailedCourse?.name]);


  const onDeleteCourse = (courseId, teamId, teacherId) => {
    getHomeworksByCourse(courseId)
        .then((courseHomeworks)=>{
          console.log(courseHomeworks);

          deleteCourse(courseId, teamId, teacherId, courseHomeworks)
              .then(() => {
                navigate('/home');
              })
              .catch((e) => {
                console.log(e.message);
              });
        });
  };


  return (
    <>
      {detailedCourse ?
        <Card className='d-flex flex-column justify-content-center align-items-center mt-5 mb-5 bg-dark text-white course-detailed-card'>
          <Card.Body >
            {detailedCourseUser ?
              <>
                <Card.Img style={{width: '10rem', height: '13rem'}} variant="top" src={detailedCourse.url} />
                <Card.Title className="fs-5 mt-4">Teacher Name:</Card.Title>
                <Card.Text>{`${detailedCourseUser.firstName} ${detailedCourseUser.lastName}`}</Card.Text>
                <Card.Title className="fs-5 mt-4">Course Name:</Card.Title>
                <Card.Text>{detailedCourse.name}</Card.Text>
                <Card.Title className="fs-5 mt-4">Course Objectives:</Card.Title>
                <Card.Text>{detailedCourse.objectives}</Card.Text>
                <Button className='mt-4' variant='primary' onClick={() => onDeleteCourse(detailedCourse.id, detailedCourse.teamId, detailedCourse.teacherId)}>Delete</Button>
              </> :
              null
            }
          </Card.Body>
        </Card> :
        null
      }
    </>
  );
}

export default WithContainer(DetailedCourse);
