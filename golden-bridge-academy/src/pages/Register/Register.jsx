import {useContext, useState} from 'react';
import AppContext from '../../providers/app-context';
import {useNavigate} from 'react-router-dom';
import {registerUser} from '../../services/auth.service';
import {createUserHandle, getUserByHandle} from '../../services/user.service';
import RegisterForm from '../../components/RegisterForm/RegisterForm';
import React from 'react';
import WithContainer from '../../hoc/WithContainer/WithContainer';
import {createStudent} from '../../services/student.service';
import {createTeacher} from '../../services/teacher.service';
import {toast} from 'react-toastify';


const Register = () => {
  const [form, setForm] = useState({
    firstName: '',
    lastName: '',
    username: '',
    email: '',
    password: '',
    phone: '',
    role: '',
  });

  const {setContext} = useContext(AppContext);

  const navigate = useNavigate();

  const updateForm = (prop, e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };


  const register = (e) => {
    e.preventDefault();

    getUserByHandle(form.username)
        .then((snapshot) => {
          if (snapshot.exists()) {
            return alert(`User with username @${form.username} already exists!`);
          }

          return registerUser(form.email, form.password)
              .then((u) => {
                return createUserHandle(form.firstName, form.lastName, form.username, u.user.email, form.phone, form.role, u.user.uid)
                    .then(() => {
                      setContext({
                        user: u.user,
                      });

                      if (form.role === 'Student') {
                        return createStudent(u.user.uid);
                      } else if (form.role === 'Teacher') {
                        return createTeacher(u.user.uid);
                      }
                    });
              })
              .then(() => {
                navigate('/home');
                toast.success(`Welcome, ${form.firstName}`, {position: toast.POSITION.BOTTOM_RIGHT, icon: '🧙‍♂️'});
              })
              .catch((e) => {
                console.log(e.message);
              });
        })
        .catch((e) => {
          console.log(e.message);
        });
  };


  return (
    <RegisterForm update={updateForm} state={form} register={register} ></RegisterForm>
  );
};

export default WithContainer(Register);
