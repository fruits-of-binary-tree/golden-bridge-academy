import React, {useContext, useEffect, useState} from 'react';
import {useLocation, useNavigate, useParams} from 'react-router-dom';
import WithContainer from '../../hoc/WithContainer/WithContainer';
import {getCoursesById, getCoursesByTeacher} from '../../services/courses.services';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {getAllHomeworks, getHomeworksByCourse} from '../../services/homework.service';
import Container from 'react-bootstrap/esm/Container';
import AppContext from '../../providers/app-context';
import './Homeworks.css';


function Homeworks() {
  const {appState, teacherState, studentState, setLoading} = useContext(AppContext);
  const [courses, setCourses] = useState([]);
  const [homeworks, setHomeworks] = useState(null);
  const {id} = useParams();
  const navigate = useNavigate();
  const location = useLocation();


  const redirect = (id) => {
    navigate(`/${id}/create-homework`);
  };

  const redirectToDetailedHomework = (id) => {
    navigate(`/${id}/detailed-homework`);
  };

  useEffect(() => {
    setLoading(true);
    if (appState?.userData?.role === 'Student') {
      getCoursesById(id)
          .then((course) => {
            setCourses([course]);
          })
          .catch((e) => {
            console.log(e.message);
          })
          .finally(() => {
            setLoading(false);
          });
    } else if (teacherState) {
      getCoursesByTeacher(teacherState.teacherId)
          .then((courses) => {
            setCourses(courses);
          })
          .catch((e) => {
            console.log(e.message);
          })
          .finally(() => {
            setLoading(false);
          });
    }
  }, [teacherState?.teacherId]);

  useEffect(() => {
    setLoading(true);

    if (appState?.userData?.role === 'Student' && location.pathname !== '/review-homeworks') {
      getHomeworksByCourse(id)
          .then((homeworks) => {
            setHomeworks(homeworks.filter((h) => h.author === studentState?.studentId));
          })
          .catch((e) => {
            console.log(e.message);
          })
          .finally(() => {
            setLoading(false);
          });
    } else if (location.pathname === '/review-homeworks') {
      getAllHomeworks()
          .then((homeworks) => {
            setHomeworks(Object.entries(homeworks).map(([id, homework]) => ({...homework, id})).filter((h) => h.author !== studentState?.studentId));
          })
          .catch((e) => {
            console.log(e.message);
          })
          .finally(() => {
            setLoading(false);
          });
    } else {
      Promise.all(courses?.map((c) => getHomeworksByCourse(c?.id)))
          .then((homeworksArr) => {
            setHomeworks(homeworksArr.flat());
          })
          .catch((e) => {
            console.log(e.message);
          })
          .finally(() => {
            setLoading(false);
          });
    }
  }, [studentState?.studentId, courses, location]);


  return (
    <Container className="bg-dark text-white rounded homework-container w-75 p-3">
      {courses && appState?.userData?.role === 'Student' && window.location.href !== 'http://localhost:3000/review-homeworks' ?
                <h2>These are your homeworks to {courses[0]?.name} course.</h2> :
                <h2>All homeworks you need to grade.</h2>
      }
      <Row>
        {homeworks?.map((homework, i) => {
          return (
            <Card style={{width: '30%', cursor: 'pointer'}} className="m-3 bg-dark text-white border-light homework-card" key={i} onClick={() => redirectToDetailedHomework(homework.id)}>
              <Card.Body>
                <Card.Title>{homework?.title}</Card.Title>
              </Card.Body>
            </Card>
          );
        })}
      </Row>
      <Col>
        {appState?.userData?.role === 'Student' && window.location.href !== 'http://localhost:3000/review-homeworks' &&
                    <Button className="m-2" variant='primary' onClick={() => redirect(courses[0].id)}>Create homework</Button>
        }
      </Col>
    </Container>
  );
}


export default WithContainer(Homeworks);
