import React, {useContext, useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import WithContainer from '../../hoc/WithContainer/WithContainer';
import AppContext from '../../providers/app-context';
import {getHomeworkById, updateDatabaseStatus} from '../../services/homework.service';
import Card from 'react-bootstrap/Card';
import Dropdown from 'react-bootstrap/Dropdown';
import './DetailedHomework.css';
import {createNotification} from '../../services/notification.services';
import {getStudentById} from '../../services/student.service';
import Comments from '../../components/Comments/Comments';
import {getUserData} from '../../services/user.service';

const status = ['Accepted', 'Rejected', 'Pending', 'Changes Requested', 'Under Review'];

function DetailedHomework() {
  const {appState, setLoading} = useContext(AppContext);
  const {id} = useParams();
  const [homework, setHomework] = useState(null);
  const [author, setAuthor] = useState(null);
  const [currentStatus, setCurrentStatus] = useState(null);

  useEffect(() => {
    setLoading(true);

    getHomeworkById(id)
        .then((homework) => {
          setHomework(homework);
        })
        .catch((e) => {
          console.log(e.message);
        })
        .finally(() => {
          setLoading(false);
        });
  }, []);


  const updateStatus = (status) => {
    setLoading(true);

    setHomework({
      ...homework,
      status: status,
    });
    setCurrentStatus(status);
  };

  useEffect(() => {
    setLoading(true);
    getStudentById(homework?.author)
        .then((student) => {
          if (student?.uid) {
            return getUserData(student.uid)
                .then((user) => {
                  setAuthor(Object.values(user?.val()));
                })
                .catch((e) => {
                  console.log(e.message);
                })
                .finally(() => {
                  setLoading(false);
                });
          }
        })
        .catch((e) => {
          console.log(e.message);
        })
        .finally(() => {
          setLoading(false);
        });
  }, [homework?.status]);

  useEffect(() => {
    setLoading(true);
    if (homework && homework.author) {
      updateDatabaseStatus(homework.status, id)
          .then(() => {
            const date = new Date();
            return getStudentById(homework.author)
                .then((student) => {
                  return createNotification(student.uid, `Status for ${homework.title}
                         changed to ${homework.status} from ${appState.userData.firstName}`, date.toISOString(), false);
                });
          })
          .catch((e) => {
            console.log(e.message);
          })
          .finally(() => {
            setLoading(false);
          });
    }
  }, [currentStatus]);


  return (
    <>
      {homework && author &&
                <div className="single-homework">
                  <Card className="homework-data bg-dark w-75 m-5">
                    <div className="detailed-homework-data d-flex justify-content-center">
                      <Card.Title className="fs-4 m-1">Title:</Card.Title>
                      <p className="m-2">{homework.title}</p>
                    </div>
                    <div className="detailed-homework-data d-flex justify-content-center">
                      <Card.Title className="fs-4 m-1">Author:</Card.Title>
                      <p className="m-2">{author[0].firstName} {author[0].lastName}</p>
                    </div>
                    <div className="detailed-homework-data d-flex justify-content-center">
                      <Card.Title className="fs-4 m-1">Content:</Card.Title>
                      <p className="m-2">{homework.content}</p>
                    </div>
                    {appState && appState?.userData?.role === 'Teacher' ?
                            <Dropdown>
                              <Dropdown.Toggle id="dropdown-autoclose-inside">{homework.status}</Dropdown.Toggle>
                              <Dropdown.Menu className="mt-1">
                                {status.map((s) => {
                                  return homework.status !== s && <Dropdown.Item key={s} onClick={() => updateStatus(s)}>{s}</Dropdown.Item>;
                                })}
                              </Dropdown.Menu>
                            </Dropdown> :
                            <div className="detailed-homework-data">
                                Status: {homework.status}
                            </div>
                    }
                    <div className="detailed-homework-comments">
                      <Comments homework={homework} setHomework={setHomework} id={id} />
                    </div>
                  </Card>
                  <embed className="homework-file" src={homework.url}></embed>
                </div>
      }
    </>
  );
}

export default WithContainer(DetailedHomework);
