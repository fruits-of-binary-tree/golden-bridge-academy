import React, {useContext, useState} from 'react';
import WithContainer from '../../hoc/WithContainer/WithContainer';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import './CreateHomework.css';
import {useNavigate, useParams} from 'react-router-dom';
import {addHomework} from '../../services/homework.service';
import AppContext from '../../providers/app-context';
import {createNotification} from '../../services/notification.services';
import {getCoursesById} from '../../services/courses.services';
import {getTeacherById} from '../../services/teacher.service';
import {toast} from 'react-toastify';


const supportedFiles = ['application/pdf', 'image/jpeg', 'image/png', 'text/javascript', 'text/html', 'text/plain'];

function CreateHomework() {
  const {studentState, appState, setLoading} = useContext(AppContext);
  const {id} = useParams();
  const navigate = useNavigate();


  const [homeworkForm, setHomeworkForm] = useState({
    title: '',
    content: '',
    file: '',
  });

  const updateHomeworkForm = (prop, e) => {
    e.preventDefault();

    if (prop === 'title' || prop === 'content') {
      setHomeworkForm({
        ...homeworkForm,
        [prop]: e.target.value,
      });
    } else {
      setHomeworkForm({
        ...homeworkForm,
        [prop]: e.target?.files[0],
      });
    }
  };


  const createHomework = () => {
    setLoading(true);

    appState && appState.userData && appState.userData.firstName &&
            addHomework(homeworkForm.title, homeworkForm.content, homeworkForm.file, studentState.studentId, id)
                .then((homeworkId) => {
                  getCoursesById(id)
                      .then((course) => {
                        getTeacherById(course.teacherId)
                            .then((teacher) => {
                              const date = new Date();
                              const url = `/${homeworkId}/detailed-homework`;

                              toast.success(`You succesffully submitted your homework `, {position: toast.POSITION.BOTTOM_RIGHT, icon: '🧙‍♂️'});
                              createNotification(teacher.uid, `${homeworkForm.title} homework added from ${appState.userData.firstName}`, date.toISOString(), false, url);
                            });
                        navigate('/home');
                      });
                })
                .catch((e) => {
                  console.log(e.message);
                })
                .finally(() => {
                  setLoading(false);
                });
  };


  return (
    <>
      <Form className="homework-form bg-dark text-white">

        <Form.Group controlId='formTitle'>
          <Form.Label>Title</Form.Label>
          <Form.Control value={homeworkForm.title} onChange={(e) => updateHomeworkForm('title', e)} type='text' placeholder='Your title goes here'></Form.Control>
        </Form.Group>

        <Form.Group controlId='formContent'>
          <Form.Label>Content</Form.Label>
          <Form.Control value={homeworkForm.content} onChange={(e) => updateHomeworkForm('content', e)} type='text' placeholder='Your content goes here'></Form.Control>
        </Form.Group>

        <Form.Group controlId='formUrl'>
          <Form.Label>Homework</Form.Label>
          <Form.Control onChange={(e) => updateHomeworkForm('file', e)} type='file' placeholder='Your homework goes here'></Form.Control>
        </Form.Group>

        <p>Supported files: {supportedFiles.map((f) => f.split('/')[1]).join(', ')}</p>
        <Button className="mt-4" disabled={!supportedFiles.includes(homeworkForm.file.type)} variant='primary' onClick={createHomework}>Submit homework</Button>
      </Form>

    </>
  );
}

export default WithContainer(CreateHomework);
