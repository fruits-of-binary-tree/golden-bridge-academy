import React, {useContext, useEffect} from 'react';
import AppContext from '../../providers/app-context';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import WithContainer from '../../hoc/WithContainer/WithContainer';
import Button from 'react-bootstrap/Button';
import {deleteNotification, setNotificationsAsSeen} from '../../services/notification.services';
import {useNavigate} from 'react-router-dom';


const Notifications = () => {
  const {notificationsState, setNotificationsState, teacherState, setLoading} = useContext(AppContext);
  const navigate = useNavigate();

  useEffect(() => {
    setLoading(true);
    setNotificationsAsSeen(notificationsState)
        .catch((e) => {
          console.log(e.message);
        })
        .finally(() => {
          setLoading(false);
        });
  }, []);

  const onDeleteNotification = (notificationId, e) => {
    e.stopPropagation();
    deleteNotification(notificationId)
        .then(() => {
          const filteredArray = notificationsState.filter((notification) => notification.id !== notificationId);
          setNotificationsState(filteredArray);
        });
  };

  const goToHomework = (url) => {
    if (teacherState) {
      navigate(url);
    } else {
      navigate('/home');
    }
  };


  return (
    <>
      <Row className='mb-5'>
        <div className='fs-2 text-white mt-5 mb-3'>Notifications</div>
        <Card className='d-flex flex-column justify-content-center align-items-center m-2 bg-dark text-white'>
          {notificationsState && notificationsState.map((notification, i) => {
            const miliseconds = Date.parse(notification.date);

            const year = new Date(miliseconds).getFullYear();
            const month = ('0' + (new Date(miliseconds).getMonth() + 1)).slice(-2);
            const day = ('0' + new Date(miliseconds).getDate()).slice(-2);

            return (
              <div onClick={() => goToHomework(notification.url)} key={i} className='border m-4 p-2 rounded' style={{width: '35rem', cursor: 'pointer'}}>
                <div className='d-flex align-items-center justify-content-center'>
                  <Card.Title className="fs-5 mt-1" > Message:</Card.Title>
                  <Card.Text className='mx-2'>{notification.message}</Card.Text>
                </div>
                <div className='d-flex align-items-center justify-content-center mt-2'>
                  <Card.Title className="fs-5 mt-1">Date:</Card.Title>
                  <Card.Text className='mx-2'>{year}/{month}/{day}</Card.Text>
                </div>
                <Button className='mt-3' onClick={(e) => onDeleteNotification(notification.id, e)}>Delete</Button>
              </div>
            );
          })
          }
        </Card >
      </Row>
    </>
  );
};


export default WithContainer(Notifications);
