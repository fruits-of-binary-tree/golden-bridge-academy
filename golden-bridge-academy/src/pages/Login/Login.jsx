import React, {useContext, useState} from 'react';
import AppContext from '../../providers/app-context';
import {loginUser} from '../../services/auth.service';
import {useNavigate} from 'react-router-dom';
import {getUserData} from '../../services/user.service';
import './Login.css';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import WithContainer from '../../hoc/WithContainer/WithContainer';


function Login() {
  const [form, setForm] = useState({
    email: '',
    password: '',
  });

  const {setContext} = useContext(AppContext);
  const navigate = useNavigate();

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const login = (e) => {
    e.preventDefault();

    loginUser(form.email, form.password)
        .then((u) => {
          return getUserData(u.user.uid)
              .then((snapshot) => {
                if (snapshot.exists()) {
                  setContext({
                    user: u.user,
                    userData: snapshot.val()[Object.keys(snapshot.val())[0]],
                  });
                  navigate('/home');
                }
              });
        })
        .catch((e) => {
          console.log(e.message);
        });
  };
  return (
    <>
      <div className="d-flex justify-content-center">
        <div className="md-auto">
          <Form className="login-form">
            <Form.Group controlId='formEmail'>
              <Form.Label>Email</Form.Label>
              <Form.Control value={form.email} onChange={updateForm('email')} type='email' placeholder='example@email.com'></Form.Control>
            </Form.Group>
            <Form.Group controlId='formPassword'>
              <Form.Label>Password</Form.Label>
              <Form.Control value={form.password} onChange={updateForm('password')} type='password' placeholder='Enter your password here'></Form.Control>
            </Form.Group>
            <div className="text-center">
              <Button className="mt-4" onClick={login} variant='primary' type='submit'>Login</Button>
            </div>
          </Form>
        </div>
      </div >
    </>
  );
}

export default WithContainer(Login);

