import React, {useContext, useEffect, useState} from 'react';
import Form from 'react-bootstrap/Form';
import {addCourse} from '../../services/courses.services';
import './CreateCourse.css';
import Button from 'react-bootstrap/Button';
import AppContext from '../../providers/app-context';
import {useNavigate} from 'react-router-dom';
import {getAllTeams} from '../../services/teams.services';
import WithContainer from '../../hoc/WithContainer/WithContainer';
import Card from 'react-bootstrap/Card';
import {toast} from 'react-toastify';


function CreateCourse() {
  const {teacherState, setLoading} = useContext(AppContext);
  const navigate = useNavigate();
  const [allClasses, setAllClasses] = useState([]);
  const [createCourseForm, setCreateCourseForm] = useState({
    teacherId: '',
    name: '',
    objectives: '',
    teamId: '',
    image: '',
  });

  const updateForm = (prop, e) => {
    e.preventDefault();

    setCreateCourseForm({
      ...createCourseForm,
      [prop]: e.target.value,
    });
  };

  const updateFormImage = (prop, e) => {
    e.preventDefault();

    setCreateCourseForm({
      ...createCourseForm,
      [prop]: e.target?.files[0],
    });
  };


  const createCourse = (e) => {
    e.preventDefault();
    if (teacherState && teacherState.teacherId) {
      addCourse(teacherState.teacherId, createCourseForm.name, createCourseForm.objectives, createCourseForm.teamId, createCourseForm.image)
          .then(() => {
            navigate('/home');
            toast.success(`You succesffully created ${createCourseForm.name} `, {position: toast.POSITION.BOTTOM_RIGHT, icon: '🧙‍♂️'});
          });
    }
  };

  useEffect(() => {
    setLoading(true);
    getAllTeams()
        .then((teams) => {
          setAllClasses(teams);
        })
        .catch((e) => {
          console.log(e.message);
        })
        .finally(() => {
          setLoading(false);
        });
  }, []);


  return (
    <Card className='bg-dark p-4'>
      <Form className='create-course-form'>
        <Form.Group controlId='formFirstName'>
          <Form.Label className='text-white'>Name</Form.Label>
          <Form.Control className='border border-dark' value={createCourseForm.name} onChange={(e) => updateForm('name', e)} type='text' placeholder='Name of the course goes here'></Form.Control>
        </Form.Group>
        <Form.Group className='d-flex flex-column' controlId='formFirstName'>
          <Form.Label className='text-white'>Objectives</Form.Label>
          <textarea className='border border-dark rounded' value={createCourseForm.objectives} onChange={(e) => updateForm('objectives', e)} type='text' placeholder='Objectives of the course goes here'></textarea>
        </Form.Group>

        <Form.Select onChange={(e) => updateForm('teamId', e)} className='mt-2' aria-label="Default select example">
          <option>Choose Team</option>
          {allClasses.map((c, i) => <option value={c.id} as='button' key={i}>{c.name}</option>)}
        </Form.Select>

        <Form.Group controlId='formUrl'>
          <Form.Label className='text-white'>Course Image</Form.Label>
          <Form.Control onChange={(e) => updateFormImage('image', e)} type='file' placeholder='Course image goes here'></Form.Control>
        </Form.Group>

        <div className='text-center'>
          <Button className="mt-2" onClick={(e) => createCourse(e)} variant='primary' type='submit'>Create Course</Button>
        </div>
      </Form>
    </Card>
  );
}


export default WithContainer(CreateCourse);


