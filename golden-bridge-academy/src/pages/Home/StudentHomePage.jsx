import React, {useContext, useEffect, useState} from 'react';
import './StudentHomePage.css';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import AppContext from '../../providers/app-context';
import WithContainer from '../../hoc/WithContainer/WithContainer';
import {getCoursesById} from '../../services/courses.services';
import {getTeamById} from '../../services/teams.services';
import {useNavigate} from 'react-router-dom';

function StudentHomePage() {
  const {studentState, setLoading} = useContext(AppContext);
  const [studentCourses, setStudentCourses] = useState(null);
  const navigate = useNavigate();

  const redirect = (id) => {
    navigate(`/courses/${id}/homeworks`);
  };

  useEffect(() => {
    setLoading(true);
    if (studentState?.studentData?.teamId) {
      const studentTeamId = studentState.studentData.teamId;

      getTeamById(studentTeamId)
          .then((studentTeam) => {
            if (studentTeam && studentTeam.courses) {
              const coursesIds = Object.keys(studentTeam.courses);
              Promise.all(coursesIds.map((id) => getCoursesById(id)))
                  .then((coursesArr) => {
                    setStudentCourses(coursesArr);
                  });
            }
          })
          .catch((e) => {
            console.log(e.message);
          })
          .finally(() => {
            setLoading(false);
          });
    }
  }, [studentState?.studentData?.teamId]);


  return (
    <div className="d-flex flex-column w-75">
      <Row>
        <Row className="d-flex justify-content-center align-items-center ">
          {studentCourses ?
                        studentCourses.map((course, i) => {
                          return (
                            <Card style={{width: '23%', height: '22rem', cursor: 'pointer'}} className="m-4 bg-dark text-white" key={i}>
                              <Card.Img style={{height: '15rem'}} src={course.url} className="p-2" />
                              <div style={{width: '100%'}} className="d-flex justify-content-between align-items-center flex-column">
                                <Card.Title className="fs-6" style={{height: '2rem'}}>{course.name}</Card.Title>
                                <button type="button" onClick={() => redirect(course.id)} className="btn btn-sm btn-outline-secondary text-white">View Homeworks</button>
                              </div>
                            </Card>
                          );
                        }) :
                        <h2 className="text-white">You are not part of any courses yet!</h2>}
        </Row>
      </Row>
    </div>

  );
}


export default WithContainer(StudentHomePage);
