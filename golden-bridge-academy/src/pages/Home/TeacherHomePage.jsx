import React, {useContext, useEffect, useState} from 'react';
import {useNavigate} from 'react-router-dom';
import './TeacherHomePage.css';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import AppContext from '../../providers/app-context';
import WithContainer from '../../hoc/WithContainer/WithContainer';
import {getCoursesByTeacher} from '../../services/courses.services';
import Button from 'react-bootstrap/Button';
import Search from '../../components/Search/Search';
import {getUsersByReviewers} from '../../services/user.service';


function TeacherHomePage() {
  const {appState, setLoading} = useContext(AppContext);
  const {teacherState} = useContext(AppContext);
  const [reviewersAdded, setReviewersAdded] = useState(0);
  const [students, setStudents] = useState([]);
  const [allCourses, setAllCourses] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    setLoading(true);
    if (teacherState?.teacherId) {
      getCoursesByTeacher(teacherState.teacherId)
          .then((courses) => {
            setAllCourses(courses);
          })
          .catch((e) => {
            console.log(e.message);
          })
          .finally(() => {
            setLoading(false);
          });
    }
  }, [teacherState?.teacherId]);

  useEffect(() => {
    setLoading(true);

    getUsersByReviewers(false)
        .then((allStudents) => {
          if (allStudents?.val()) {
            setStudents(Object.values(allStudents?.val()));
          } else {
            setStudents([]);
          }
        })
        .finally(() => {
          setLoading(false);
        });
  }, [reviewersAdded]);

  const createCourse = () => {
    navigate('/teachers/id/create-course');
  };

  const redirect = (id) => {
    navigate(`/courses/${id}/detailed-course`);
  };


  return (
    <div className="d-flex flex-column w-75">
      <Row className="mt-3">
        <h5 className="text-white mb-3 mt-5 home-headings">Add Prefect</h5>
        <Card className='bg-dark mb-5 mt-1 pt-3'>
          <Search students={students} setStudents={setStudents} userRole={appState.userData?.role} setReviewersAdded={setReviewersAdded}/>
        </Card>
        <h5 className="text-white mb-0 mt-5 home-headings">Your Courses</h5>
        <Row className="d-flex justify-content-center align-items-center">
          {allCourses && allCourses.map((course, i) => {
            return (
              <Card key={i} style={{width: '23%', height: '25rem', cursor: 'pointer'}} className="m-4 bg-dark text-white">
                <Card.Img className="p-2" src={course.url} style={{height: '15rem'}}></Card.Img>
                <p className="card-text overflow-hidden" style={{height: '15rem'}}>{course.objectives}</p>
                <div className="d-flex justify-content-between align-items-center">
                  <div style={{width: '100%'}} className="d-flex justify-content-between align-items-center">
                    <button type="button" onClick={() => redirect(course.id)} className="btn btn-sm btn-outline-secondary text-white">View</button>
                    <Card.Title className="home-course-name" style={{height: '2rem'}}>{course.name}</Card.Title>
                  </div>
                </div>
              </Card>
            );
          })}
          <div className="center-text m-5">
            <Button onClick={createCourse}>Create Course</Button >
          </div>
        </Row>
      </Row >
    </div >
  );
}


export default WithContainer(TeacherHomePage);

