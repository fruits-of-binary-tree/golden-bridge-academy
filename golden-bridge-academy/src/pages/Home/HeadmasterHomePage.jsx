import React, {useContext, useEffect, useState} from 'react';
import './HeadmasterHomePage.css';
import {AiOutlineUserDelete} from 'react-icons/ai';
import Container from 'react-bootstrap/Container';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import AppContext from '../../providers/app-context';
import WithContainer from '../../hoc/WithContainer/WithContainer';
import Search from '../../components/Search/Search';
import {addTeam, getAllTeams} from '../../services/teams.services';
import {getAllStudentsWithoutTeam, updateStudentData} from '../../services/student.service';
import {toast} from 'react-toastify';

const defaultFormState = {
  name: '',
  students: [],
  courses: [],
};

function HeadmasterHomePage() {
  const {appState, setLoading} = useContext(AppContext);
  const [students, setStudents] = useState([]);
  const [teams, setTeams] = useState([]);
  const [teamsCreated, setTeamsCreated] = useState(0);
  const [show, setShow] = useState(false);
  const [form, setForm] = useState(defaultFormState);
  const [formNameLabel, setFormNameLabel] = useState('');

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    setForm({
      ...form,
      students: students.filter((s) => s.isSelected),
    });
  }, [students]);

  const handleSave = () => {
    handleClose();
    setFormNameLabel(form.name);
  };

  const update = (prop, e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  useEffect(() => {
    setLoading(true);
    getAllStudentsWithoutTeam()
        .then((students) => {
          setStudents(students);
        })
        .catch((e) => {
          console.log | (e.message);
        })
        .finally(() => {
          setLoading(false);
        });
  }, [teamsCreated]);

  useEffect(() => {
    setLoading(true);
    getAllTeams()
        .then((snapshot) => {
          setTeams(snapshot);
        })
        .catch((e) => {
          console.log(e.message);
        })
        .finally(() => {
          setLoading(false);
        });
  }, [teamsCreated]);

  const handleDelete = (uid) => {
    setStudents(students.map((student) => {
      const modifiedStudent = {...student};
      if (student.uid === uid) {
        modifiedStudent.isSelected = false;
        modifiedStudent.isChecked = false;
      }
      return modifiedStudent;
    }));
  };

  const createTeam = () => {
    setLoading(true);
    console.log('cksndj');
    addTeam(form.name, form.students.map((s) => s.uid), form.courses)
        .then((result) => Promise.all(form.students.map((s) => updateStudentData(s.studentId, {
          teamId: result.key,
        }),
        )))
        .then(() => {
          setTeamsCreated((t) => t + 1);
          setForm(defaultFormState);
          toast.success(`You succesffully created ${formNameLabel}`, {position: toast.POSITION.BOTTOM_RIGHT, icon: '🧙‍♂️'});
          setFormNameLabel('');
        })
        .catch((e) => {
          console.log(e.message);
        })
        .finally(() => {
          setLoading(false);
        });
  };

  return (
    <Container style={{width: '95%'}}>
      <Row className="headmaster-container">
        <Col className="bg-dark m-2 p-3 rounded">
          <div className="classes">
            <h4>List of all teams</h4>
            <ul>
              <Row>
                {teams?.map((el, i) => {
                  return (
                    <Card style={{width: '40%'}} className="mt-3 m-3 bg-dark text-white border border-light p-2" key={i}>
                      <Card.Title className="bg-dark text-white">{el.name}</Card.Title>
                    </Card>
                  );
                })}
              </Row>
            </ul>
          </div>
          <Button variant='primary' onClick={handleShow}>Create new team</Button>
          <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Write a name of the new team</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form.Control value={form.name} onChange={(e) => update('name', e)} type='text' placeholder='Team name...'></Form.Control>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button variant="primary" onClick={handleSave}>
                Save Changes
              </Button>
            </Modal.Footer>
          </Modal>
        </Col>
        <Col className="bg-dark m-2 p-3 rounded">
          <Search students={students} setStudents={setStudents} userRole={appState.userData?.role} />
          <Container className="members-container bg-dark text-white">
            <div>
              <h4>{formNameLabel}</h4>
              <ul>
                <Row>
                  {students?.map((student) => {
                    return student.isSelected &&
                      (
                        <div className="members" key={student.uid}>
                          <p>{student.firstName}</p>
                          <i onClick={() => handleDelete(student.uid)}><AiOutlineUserDelete /></i>
                        </div>
                      );
                  })}
                </Row>
              </ul>
            </div>
          </Container>
          <Button className="mb-3" variant='primary' onClick={createTeam}>Create</Button>
        </Col>
      </Row>
    </Container >
  );
}


export default WithContainer(HeadmasterHomePage);
