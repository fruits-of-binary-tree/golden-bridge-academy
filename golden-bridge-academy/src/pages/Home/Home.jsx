import React from 'react';
import sortingHat from '../../images/sorting-hat.gif';
import Button from 'react-bootstrap/Button';
import {useNavigate} from 'react-router-dom';
import dementor from '../../images/dementor.gif';
import harry from '../../images/harry.gif';
import mail from '../../images/mail.gif';
import map from '../../images/map.gif';
import './Home.css';
import WithContainer from '../../hoc/WithContainer/WithContainer';

function Home() {
  const navigate = useNavigate();

  const redirect = () => {
    navigate('/register');
  };

  return (
    <div className="home">
      <div className="text">
        <h2>In Golden Bridge Academy teaching and learning come together!</h2>
        <h5>GBA is your all-in-one place for reviewing, assigning and receiving tasks.</h5>
      </div>
      <div className="button">
        <Button variant='primary' className="mt-5" onClick={redirect}>Get started</Button>
      </div>
      <div className="icons">
        <div>
          <img src={mail}></img>
          <h4>All-in-one place</h4>
          <p>Bring all your learning tools together and manage multiple courses in one central destination.</p>
        </div>
        <div>
          <img src={harry}></img>
          <h4>Easy to use</h4>
          <p>Anyone in your school community can get up and running with GBA in minutes.</p>
        </div>
        <div>
          <img src={dementor}></img>
          <h4>Build for collaboration</h4>
          <p>You have opportunity to become prefect and review your teammates homeworks.</p>
        </div>
        <div>
          <img src={map}></img>
          <h4>Access from anywhere</h4>
          <p>Empower teaching and learning from anywhere, on any device, and give your class more flexibility and mobility.</p>
        </div>
      </div>
      <div className="use mt-5">
        <div className="list">
          <h2>Save time and simplify everyday tasks</h2>
          <ul>
            <li>Switch from class to assignment to student in just a few clicks</li>
            <li>Track student progress depending on their submitted work</li>
            <li>Assign different tasks to a specific student and review their work</li>
            <li>Create courses with all the students in the same class</li>
            <li>Send notifications if the task is completed successfully or return it for change</li>
          </ul>
        </div>
        <img className="mb-5" src={sortingHat} />
      </div>
    </div>
  );
}

export default WithContainer(Home);
