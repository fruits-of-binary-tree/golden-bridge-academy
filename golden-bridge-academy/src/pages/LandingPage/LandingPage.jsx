import React, {useEffect, useState} from 'react';
import './LandingPage.css';
import Fade from 'react-bootstrap/Fade';
import {useNavigate} from 'react-router';


function LandingPage() {
  const [isOpen, setIsOpen] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    setIsOpen(!isOpen);
  }, []);

  const redirect = () => {
    navigate('/home');
  };

  return (
    <Fade in={isOpen}>
      <div className='fade-container'>
        <img className='logo' onClick={redirect} src="https://www.freepnglogos.com/uploads/hogwarts-logo-png/logo-hogwarts-hogwartsessence-deviantart-19.png" width="50" alt="logo hogwarts" />
      </div>
    </Fade>
  );
}

export default LandingPage;

