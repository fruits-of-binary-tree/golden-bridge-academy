import React, {useContext, useState} from 'react';
import {storage} from '../../firebase-config';
import {getDownloadURL, ref, uploadBytes} from 'firebase/storage';
import AppContext from '../../providers/app-context';
import {updateUserData, updateUserProfilePicture} from '../../services/user.service';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import './Profile.css';
import WithContainer from '../../hoc/WithContainer/WithContainer';
import {BsCamera} from 'react-icons/bs';
import {AiOutlineUser} from 'react-icons/ai';
import {HiOutlineMail} from 'react-icons/hi';
import {AiOutlinePhone} from 'react-icons/ai';
import {SiGoogleclassroom} from 'react-icons/si';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {AiOutlineEdit} from 'react-icons/ai';
import {getTeamById} from '../../services/teams.services';
import {toast} from 'react-toastify';


export const Profile = () => {
  const {setContext, appState, studentState} = useContext(AppContext);
  const [profilePicture, setProfilePicture] = useState(null);
  const [showInput, setShowInput] = useState(false);
  const [editMode, setEditMode] = useState(false);
  const [studentTeam, setStudentTeam] = useState(null);
  const [userProfileForm, setUserProfileForm] = useState({
    firstName: appState.userData.firstName,
    lastName: appState.userData.lastName,
    email: appState.userData.email,
    phone: appState.userData.phone,
  });


  if (appState.userData) {
    if (appState.userData.role === 'Student') {
      getTeamById(studentState.studentData.teamId)
          .then((team) => {
            setStudentTeam(team.name);
          });
    }
  }

  const updateForm = (prop, e) => {
    setUserProfileForm({
      ...userProfileForm,
      [prop]: e.target.value,
    });
  };


  const uploadPhoto = (e) => {
    e.preventDefault();

    if (profilePicture === null) {
      return;
    }

    const photoRef = ref(storage, `profileImages/${profilePicture.name}`);

    uploadBytes(photoRef, profilePicture)
        .then((snapshot) => {
          return getDownloadURL(snapshot.ref)
              .then((url) => {
                return updateUserProfilePicture(appState?.userData?.handle, url)
                    .then(() => {
                      setContext({
                        ...appState,
                        userData: {
                          ...appState?.userData,
                          profilePicture: url,
                        },
                      });

                      toast.success(`You successfully updated your wizard picture`, {position: toast.POSITION.BOTTOM_RIGHT, icon: '🧙‍♂️'});
                    });
              });
        });
  };

  const updatePhoto = (e) => {
    e.preventDefault;
    setProfilePicture(e.target?.files[0]);
  };

  const showInputForPhoto = (e) => {
    e.preventDefault;
    setShowInput(true);
  };

  const editProfile = () => {
    setEditMode(true);
  };

  const cancelOnClick = (e) => {
    e.preventDefault();
    setEditMode(false);
  };

  const updateUserProfile = () => {
    setEditMode(false);

    updateUserData(appState.userData.handle, {
      'firstName': userProfileForm.firstName,
      'lastName': userProfileForm.lastName,
      'email': userProfileForm.email,
      'phone': userProfileForm.phone,
    }).then(() => {
      setEditMode(false);

      setContext({
        ...appState,
        userData: {
          ...appState.userData,
          firstName: userProfileForm.firstName,
          lastName: userProfileForm.lastName,
          email: userProfileForm.email,
          phone: userProfileForm.phone,
        },
      });
      toast.success(`You successfully updated your wizard info`, {position: toast.POSITION.BOTTOM_RIGHT, icon: '🧙‍♂️'});
    })
        .catch((e) => {
          console.log(e.message);
        });
  };


  return (
    <>
      <section className=' bg-dark text-white p-3 text-center main-card rounded'>
        <div className='container'>
          {/* edit mode */}
          {editMode ?
            <Form className='profile-edit-mode'>
              <Form.Group controlId='formFirstName'>
                <Form.Label>First Name</Form.Label>
                <Form.Control onChange={(e) => updateForm('firstName', e)} value={userProfileForm.firstName}></Form.Control>
              </Form.Group>
              <Form.Group controlId='formLastName'>
                <Form.Label>Last Name</Form.Label>
                <Form.Control onChange={(e) => updateForm('lastName', e)} value={userProfileForm.lastName}></Form.Control>
              </Form.Group>
              <Form.Group controlId='formEmail'>
                <Form.Label>Email</Form.Label>
                <Form.Control onChange={(e) => updateForm('email', e)} value={userProfileForm.email}></Form.Control>
              </Form.Group>
              <Form.Group controlId='formPhone'>
                <Form.Label>Phone Number</Form.Label>
                <Form.Control onChange={(e) => updateForm('phone', e)} value={userProfileForm.phone}></Form.Control>
              </Form.Group>
              <div className='d-flex justify-content-evenly mt-3'>
                <Button onClick={(e) => updateUserProfile(e)}>Save</Button>
                <Button onClick={(e) => cancelOnClick(e)}>Cancel</Button>
              </div>
            </Form> :
            <>
              {/* not-edit mode */}
              <div className='d-flex justify-content-around align-items-center'>
                <div>
                  <div className='bg-dark text-white'>
                    <>
                      {appState && appState.userData && appState.userData.profilePicture ?
                        <div className='profile-card-img-container text-center ' >
                          <img className='profile-img' src={appState.userData.profilePicture} ></img >
                        </div> :
                        < Form.Group controlId='formPhoto' >
                          <Form.Control className={!showInput && 'profile-card__input'} onChange={(e) => updatePhoto(e)} type='file'></Form.Control>
                          <div className='text-center p-3'>
                            <BsCamera size={50} className={showInput && 'profile-card__input'} onClick={(e) => showInputForPhoto(e)} />
                          </div>
                          <Button className={!showInput && 'profile-card__input'} onClick={(e) => uploadPhoto(e)}>Upload</Button>
                        </Form.Group >
                      }
                    </>
                  </div>
                </div>
                <div className='mx-5'>
                  {appState && appState.userData && appState.userData ?
                    <>
                      <h5>{appState.userData.role}</h5>
                      <h1>{`${appState.userData.firstName} ${appState.userData.lastName}`}</h1>
                    </> :
                    null
                  }
                </div>
              </div>
              <Row>
                <Col>
                  <div className='d-flex flex-column align-items-start mt-5'>
                    {appState && appState.userData && appState.userData ?
                      <>
                        <p><u><AiOutlineUser size={20} className='m-2' />{`${appState.userData.handle}`}</u></p>
                        <p><u><HiOutlineMail size={20} className='m-2' />{`${appState.userData.email}`}</u></p>
                        <p><u><AiOutlinePhone size={20} className='m-2' />{`${appState.userData.phone}`}</u></p>
                        {studentTeam ?
                          <p><u><SiGoogleclassroom size={20} className='m-2' />{studentTeam}</u></p> :
                          null
                        }
                      </> :
                      null
                    }
                  </div>
                </Col>
                <Col className='d-flex align-items-end justify-content-end'>
                  <AiOutlineEdit onClick={editProfile} size={20} />
                </Col>
              </Row>
            </>
          }
        </div>
      </section>
    </>
  );
};

export default WithContainer(Profile);


