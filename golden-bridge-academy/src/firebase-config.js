import {initializeApp} from 'firebase/app';
import {getAuth} from 'firebase/auth';
import {getDatabase} from 'firebase/database';
import {getStorage} from 'firebase/storage';


const firebaseConfig = {
  apiKey: 'AIzaSyDonz8BJc-CwoT4vJD7vuRAj5hq168hR5A',
  authDomain: 'golden-bridge-academy.firebaseapp.com',
  databaseURL: 'https://golden-bridge-academy-default-rtdb.europe-west1.firebasedatabase.app/',
  projectId: 'golden-bridge-academy',
  storageBucket: 'golden-bridge-academy.appspot.com',
  messagingSenderId: '681910257584',
  appId: '1:681910257584:web:63eaff59546ba36fa83875',
};

export const app = initializeApp(firebaseConfig);
// the Firebase authentication handler
export const auth = getAuth(app);
// the Realtime Database handler
export const db = getDatabase(app);

export const storage = getStorage(app);


